# al-arch-repo

An arch repository wich contains extra software prebuilt for you

A link to my AUR account :

<a href="https://aur.archlinux.org/account/Alemswaj">
  <img src="https://aur.archlinux.org/css/archnavbar/aurlogo.png" />
  </a>

If you want to use this repository, add the following line in `/etc/pacman.conf`

<code>[al-arch-repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/alemswaj/al-arch-repo/-/raw/main/x86_64</code>
